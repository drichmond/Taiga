/*
 * Copyright © 2017 Eric Matthews,  Lesley Shannon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Initial code developed under the supervision of Dr. Lesley Shannon,
 * Reconfigurable Computing Lab, Simon Fraser University.
 *
 * Author(s):
 *             Eric Matthews <ematthew@sfu.ca>
 */
 
module one_hot_to_integer
        #(
        parameter C_WIDTH = 32
        )
        (
        input logic [C_WIDTH-1:0] one_hot,
        output logic [$clog2(C_WIDTH)-1:0] int_out
        );

    always_comb begin
        int_out = 0;
        for (int i=1; i < C_WIDTH; i=i+1)
            if (one_hot[i]) int_out = i;
    end

endmodule
